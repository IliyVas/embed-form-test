class StaticPagesController < ApplicationController
  def home
    response.headers.delete "X-Frame-Options"
  end
end
